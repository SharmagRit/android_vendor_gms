# All unpinned files are extracted from raven UQ1A.240205.002

# GMS mandatory core packages
# GoogleCalendarSyncAdapter, not exisiting on pixel because it is overridden by CalendarGooglePrebuilt
product/app/GoogleTTS/GoogleTTS.apk;OVERRIDES=PicoTts;PRESIGNED
product/app/LocationHistoryPrebuilt/LocationHistoryPrebuilt.apk;PRESIGNED
product/app/WebViewGoogle-Stub/WebViewGoogle-Stub.apk;OVERRIDES=webview;PRESIGNED
product/app/WebViewGoogle/WebViewGoogle.apk.gz
product/priv-app/AndroidAutoStubPrebuilt/AndroidAutoStubPrebuilt.apk;PRESIGNED
product/priv-app/ConfigUpdater/ConfigUpdater.apk;PRESIGNED
product/priv-app/GoogleOneTimeInitializer/GoogleOneTimeInitializer.apk;OVERRIDES=OneTimeInitializer;PRESIGNED
product/priv-app/GoogleRestorePrebuilt/GoogleRestorePrebuilt.apk;PRESIGNED
product/priv-app/PartnerSetupPrebuilt/PartnerSetupPrebuilt.apk;PRESIGNED
product/priv-app/Phonesky/Phonesky.apk;PRESIGNED
product/priv-app/PrebuiltGmsCore/m/independent/AndroidPlatformServices.apk;PRESIGNED
product/priv-app/PrebuiltGmsCore/PrebuiltGmsCoreSc.apk;OVERRIDES=NetworkRecommendation;PRESIGNED
product/priv-app/SetupWizardPrebuilt/SetupWizardPrebuilt.apk;OVERRIDES=Provision;PRESIGNED
product/priv-app/WellbeingPrebuilt/WellbeingPrebuilt.apk;PRESIGNED
system/app/GoogleExtShared/GoogleExtShared.apk;OVERRIDES=ExtShared;PRESIGNED
system/app/GooglePrintRecommendationService/GooglePrintRecommendationService.apk;OVERRIDES=PrintRecommendationService;PRESIGNED
system_ext/priv-app/GoogleServicesFramework/GoogleServicesFramework.apk;PRESIGNED

# GMS mandatory application packages
# Duo, not existing on pixel
product/app/Chrome-Stub/Chrome-Stub.apk;OVERRIDES=Browser,Browser2,Jelly;PRESIGNED
product/app/Chrome/Chrome.apk.gz
product/app/Photos/Photos.apk;OVERRIDES=Gallery2,SnapdragonGallery;PRESIGNED
product/app/TrichromeLibrary-Stub/TrichromeLibrary-Stub.apk;PRESIGNED
product/app/TrichromeLibrary/TrichromeLibrary.apk.gz
product/priv-app/Velvet/Velvet.apk;OVERRIDES=QuickSearchBox;PRESIGNED

# GMS optional application packages
# Keep, not existing on pixel
product/app/CalculatorGooglePrebuilt/CalculatorGooglePrebuilt.apk;OVERRIDES=Calculator,ExactCalculator;PRESIGNED
product/app/CalendarGooglePrebuilt/CalendarGooglePrebuilt.apk;OVERRIDES=Calendar,GoogleCalendarSyncAdapter,Etar;PRESIGNED
product/app/LatinIMEGooglePrebuilt/LatinIMEGooglePrebuilt.apk;OVERRIDES=LatinIME;PRESIGNED
product/app/PrebuiltDeskClockGoogle/PrebuiltDeskClockGoogle.apk;OVERRIDES=AlarmClock,DeskClock;PRESIGNED
product/app/talkback/talkback.apk;PRESIGNED
product/priv-app/FilesPrebuilt/FilesPrebuilt.apk;PRESIGNED
system/priv-app/TagGoogle/TagGoogle.apk;OVERRIDES=Tag;PRESIGNED
product/priv-app/AmbientStreaming/AmbientStreaming.apk;PRESIGNED
product/priv-app/HealthConnectPrebuilt/HealthConnectPrebuilt.apk;PRESIGNED

# GMS comms suite
# CarrierServices, existing on pixel but not used in favor of CarrierConfig
product/app/GoogleContacts/GoogleContacts.apk;OVERRIDES=Contacts;PRESIGNED
product/etc/permissions/com.google.android.dialer.support.xml
product/framework/com.google.android.dialer.support.jar;PRESIGNED
product/priv-app/GoogleDialer/GoogleDialer.apk;OVERRIDES=Dialer;PRESIGNED
product/priv-app/PrebuiltBugle/PrebuiltBugle.apk;OVERRIDES=messaging;PRESIGNED

# GMS turbo
product/priv-app/TurboPrebuilt/TurboPrebuilt.apk;PRESIGNED

# Configuration files
product/etc/default-permissions/default-permissions.xml
product/etc/default-permissions/default-permissions_maestro.xml
product/etc/permissions/privapp-permissions-google-p.xml
product/etc/permissions/split-permissions-google.xml
product/etc/preferred-apps/google.xml
product/etc/security/fsverity/gms_fsverity_cert.der
product/etc/security/fsverity/play_store_fsi_cert.der
product/etc/sysconfig/google.xml
product/etc/sysconfig/google-hiddenapi-package-whitelist.xml
product/etc/sysconfig/google-initial-package-stopped-states.xml
product/etc/sysconfig/google-install-constraints-package-allowlist.xml
product/etc/sysconfig/google-staged-installer-whitelist.xml
product/etc/sysconfig/google-system-apps-update-ownership.xml
product/etc/sysconfig/google_build.xml
product/etc/sysconfig/nexus.xml
product/etc/sysconfig/pixel_2017-initial-package-stopped-states.xml
product/etc/sysconfig/pixel_2016_exclusive.xml|f92443f76f9af566813e08495f00c37a882af824
product/etc/sysconfig/pixel_experience_2017.xml
product/etc/sysconfig/pixel_experience_2018.xml
product/etc/sysconfig/pixel_experience_2019.xml
product/etc/sysconfig/pixel_experience_2019_midyear.xml
product/etc/sysconfig/pixel_experience_2020.xml
product/etc/sysconfig/pixel_experience_2020_midyear.xml
product/etc/sysconfig/pixel_experience_2021_midyear.xml
product/etc/sysconfig/pixel_experience_2021.xml
#product/etc/sysconfig/pixel_experience_2022_midyear.xml
product/etc/sysconfig/preinstalled-packages-platform-handheld-product.xml
product/etc/sysconfig/preinstalled-packages-platform-overlays.xml
product/etc/sysconfig/preinstalled-packages-platform-telephony-product.xml
product/etc/sysconfig/preinstalled-packages-product-pixel-2017-and-newer.xml
product/etc/sysconfig/preinstalled-packages-product-pixel-2018-and-newer.xml
product/etc/sysconfig/preinstalled-packages-product-pixel-2019-and-newer.xml
system/etc/permissions/privapp-permissions-google.xml
system_ext/etc/permissions/privapp-permissions-google-se.xml

# Pixel application packages
product/app/MarkupGoogle/MarkupGoogle.apk;PRESIGNED
product/app/PixelThemesStub/PixelThemesStub.apk;OVERRIDES=ThemePicker;PRESIGNED
product/app/SoundAmplifierPrebuilt/SoundAmplifierPrebuilt.apk;PRESIGNED
product/app/SoundPickerPrebuilt/SoundPickerPrebuilt.apk;PRESIGNED
product/priv-app/AICorePrebuilt/AICorePrebuilt.apk;PRESIGNED
product/priv-app/DeviceIntelligenceNetworkPrebuilt/DeviceIntelligenceNetworkPrebuilt.apk;PRESIGNED
product/priv-app/DevicePersonalizationPrebuiltPixel2021/DevicePersonalizationPrebuiltPixel2021.apk;PRESIGNED
product/priv-app/SafetyHubPrebuilt/SafetyHubPrebuilt.apk;PRESIGNED
product/priv-app/ScribePrebuilt/ScribePrebuilt.apk;PRESIGNED
product/priv-app/SettingsIntelligenceGooglePrebuilt/SettingsIntelligenceGooglePrebuilt.apk;OVERRIDES=SettingsIntelligence;PRESIGNED
system/priv-app/DocumentsUIGoogle/DocumentsUIGoogle.apk;OVERRIDES=DocumentsUI;PRESIGNED
system_ext/app/EmergencyInfoGoogleNoUi/EmergencyInfoGoogleNoUi.apk;OVERRIDES=EmergencyInfo
system_ext/app/Flipendo/Flipendo.apk
system_ext/priv-app/StorageManagerGoogle/StorageManagerGoogle.apk;OVERRIDES=StorageManager;PRESIGNED
system_ext/priv-app/NexusLauncherRelease/NexusLauncherRelease.apk;OVERRIDES=Launcher3QuickStep,TrebuchetQuickStep;PRESIGNED
system_ext/priv-app/PixelSetupWizard/PixelSetupWizard.apk;OVERRIDES=LineageSetupWizard;PRESIGNED

# Pixel fonts
# product/etc/fonts_customization.xml
# product/fonts/GoogleSans-Italic.ttf
# product/fonts/GoogleSans-Regular.ttf
# product/fonts/GoogleSansClock-Regular.ttf

# Misc
product/usr/share/ime/google/d3_lms/ko_2018030706.zip
product/usr/share/ime/google/d3_lms/mozc.data
product/usr/share/ime/google/d3_lms/zh_CN_2018030706.zip
product/usr/srec/en-US/acousticmodel/MARBLE_DICTATION_EP.endpointer_portable_lstm_mean_stddev
product/usr/srec/en-US/acousticmodel/MARBLE_DICTATION_EP.endpointer_portable_lstm_model
product/usr/srec/en-US/acousticmodel/MARBLE_VOICE_ACTIONS_EP.endpointer_portable_lstm_mean_stddev
product/usr/srec/en-US/acousticmodel/MARBLE_VOICE_ACTIONS_EP.endpointer_portable_lstm_model
product/usr/srec/en-US/config.pumpkin
product/usr/srec/en-US/configs/ONDEVICE_MEDIUM_CONTINUOUS.config
product/usr/srec/en-US/configs/ONDEVICE_MEDIUM_SHORT_compiler.config
product/usr/srec/en-US/configs/ONDEVICE_MEDIUM_SHORT.config
product/usr/srec/en-US/context_prebuilt/apps.txt
product/usr/srec/en-US/context_prebuilt/contacts.txt
product/usr/srec/en-US/context_prebuilt/en-US_android-auto_car_automation.action.union_STD_FST.fst
product/usr/srec/en-US/context_prebuilt/en-US_android-auto_manual_fixes_STD_FST.fst
product/usr/srec/en-US/context_prebuilt/en-US_android-auto_top_radio_station_frequencies_STD_FST.fst
product/usr/srec/en-US/context_prebuilt/songs.txt
product/usr/srec/en-US/denorm/embedded_class_denorm.mfar
product/usr/srec/en-US/denorm/embedded_covid_19.mfar
product/usr/srec/en-US/denorm/embedded_fix_ampm.mfar
product/usr/srec/en-US/denorm/embedded_normalizer.mfar
product/usr/srec/en-US/denorm/embedded_replace_annotated_punct_words_dash.mfar
product/usr/srec/en-US/denorm/porn_normalizer_on_device.mfar
product/usr/srec/en-US/endtoendmodel/marble_rnnt_dictation_frontend_params.mean_stddev
product/usr/srec/en-US/endtoendmodel/marble_rnnt_model-encoder.part_0.tflite
product/usr/srec/en-US/endtoendmodel/marble_rnnt_model-encoder.part_1.tflite
product/usr/srec/en-US/endtoendmodel/marble_rnnt_model-rnnt.decoder.tflite
product/usr/srec/en-US/endtoendmodel/marble_rnnt_model-rnnt.joint.tflite
product/usr/srec/en-US/endtoendmodel/marble_rnnt_model.syms.compact
product/usr/srec/en-US/endtoendmodel/marble_rnnt_model.word_classifier
product/usr/srec/en-US/endtoendmodel/marble_rnnt_model.wpm.portable
product/usr/srec/en-US/endtoendmodel/marble_rnnt_voice_actions_frontend_params.mean_stddev
product/usr/srec/en-US/g2p
product/usr/srec/en-US/g2p_phonemes.syms
product/usr/srec/en-US/g2p.syms
product/usr/srec/en-US/hotword.data
product/usr/srec/en-US/magic_mic/MARBLE_V2_acoustic_meanstddev_vector
product/usr/srec/en-US/magic_mic/MARBLE_V2_acoustic_model.int8.tflite
product/usr/srec/en-US/magic_mic/MARBLE_V2_model.int8.tflite
product/usr/srec/en-US/magic_mic/MARBLE_V2_vocabulary.syms
product/usr/srec/en-US/metadata
product/usr/srec/en-US/monastery_config.pumpkin
product/usr/srec/en-US/offline_action_data.pb
product/usr/srec/en-US/pumpkin.mmap
product/usr/srec/en-US/semantics.pumpkin
product/usr/srec/en-US/SODA_punctuation_config.pb
product/usr/srec/en-US/SODA_punctuation_model.tflite
product/usr/srec/en-US/voice_match/MARBLE_speakerid.tflite

# Clock plugins
system_ext/priv-app/SystemUIClocks-BigNum/SystemUIClocks-BigNum.apk
system_ext/priv-app/SystemUIClocks-Calligraphy/SystemUIClocks-Calligraphy.apk
system_ext/priv-app/SystemUIClocks-Flex/SystemUIClocks-Flex.apk
system_ext/priv-app/SystemUIClocks-Growth/SystemUIClocks-Growth.apk
system_ext/priv-app/SystemUIClocks-Inflate/SystemUIClocks-Inflate.apk
system_ext/priv-app/SystemUIClocks-NumOverlap/SystemUIClocks-NumOverlap.apk
system_ext/priv-app/SystemUIClocks-Weather/SystemUIClocks-Weather.apk
system_ext/priv-app/SystemUIClocks-Metro/SystemUIClocks-Metro.apk

# samsung
product/etc/sysconfig/google_searcle.xml
